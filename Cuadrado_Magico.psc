Algoritmo Cuadrado_magico
	
	Escribir "digite la dimencion del cuadrado, teniendo en cuneta que sera n x n, y que n debe ser un numeor impar"
	
	valor_cuadrado <- -1
	
	Mientras valor_cuadrado < 3 Hacer
		Leer pre_val_cua		
		
		Si pre_val_cua % 2 == 0 o pre_val_cua < 3 Entonces
			valor_cuadrado <- -1
			
			Escribir "Verifique que el valor ingresado cumpla las condiciones necesarias, el valor debe ser impar"
			
		SiNo
			valor_cuadrado <- pre_val_cua
		Fin Si
		
	Fin Mientras
	
	
	Dimension cuadrado[valor_cuadrado, valor_cuadrado]
	
	Para fil <- 1 Hasta valor_cuadrado Con Paso 1 Hacer
		Para col <- 1  Hasta valor_cuadrado Con Paso 1 Hacer
			cuadrado[fil, col] <- 0
		Fin Para
	Fin Para
	
	pos_fil <- 1
	pos_col <- ((valor_cuadrado - 1)/2) + 1
	
	total_pasos <- (valor_cuadrado * valor_cuadrado)
	
	cuadrado[pos_fil, pos_col] <- 1
	
	Para step <- 2 Hasta total_pasos Con Paso 1 Hacer
		
		pos_fil <- pos_fil - 1
		pos_col <- pos_col + 1
		
		Si pos_fil < 1 Entonces
			pos_fil <- valor_cuadrado
		SiNo
		Fin Si
		
		Si pos_col > valor_cuadrado Entonces
			pos_col <- 1
		SiNo
		Fin Si
		
		Si cuadrado[pos_fil, pos_col]  == 0 Entonces
			cuadrado[pos_fil, pos_col] <- step
			
		SiNo
			
			pos_fil <- pos_fil + 1
			pos_col <- pos_col - 1
			
			Si pos_fil > valor_cuadrado Entonces
				pos_fil <- 1
			SiNo
			Fin Si
			
			Si pos_col < 1 Entonces
				pos_col <- valor_cuadrado
			SiNo
			Fin Si
			
			pos_fil <- pos_fil + 1
			
			cuadrado[pos_fil, pos_col] <- step
			
		Fin Si
		
		
	Fin Para
	
	
	Para fil <- 1 Hasta valor_cuadrado Con Paso 1 Hacer
		val_fila <- "["
		Para col <- 1 Hasta valor_cuadrado Con Paso 1 Hacer
			
			val_fila <- val_fila + " " + ConvertirATexto(cuadrado[fil, col]) + ", "
			
		Fin Para
		
		val_fila <- val_fila + "]"
		
		Escribir val_fila
	Fin Para
	
	Escribir "\--------------------------------------------------------------------------------------/"
	
	Para fil <- 1 Hasta valor_cuadrado Con Paso 1 Hacer
		acumilado_fila <- 0
		Para col <- 1 Hasta valor_cuadrado Con Paso 1 Hacer
			
			acumilado_fila <- acumilado_fila + cuadrado[fil, col]
			
		Fin Para
		Escribir "la suma de los valores en la fila " fil " es " acumilado_fila
	Fin Para
	
	Escribir "\--------------------------------------------------------------------------------------/"
	
	Para col <- 1 Hasta valor_cuadrado Con Paso 1 Hacer
		acumilado_fila <- 0
		Para fil <- 1 Hasta valor_cuadrado Con Paso 1 Hacer
			
			acumilado_fila <- acumilado_fila + cuadrado[fil, col]
			
		Fin Para
		Escribir "la suma de los valores en la columna " col " es " acumilado_fila
	Fin Para
	
	
FinAlgoritmo
